const tree = [
  {
    id: 1,
    name: "items 1", 
    childrens: [
      {
        id: 2,
        name: "items 1.1",
        childrens: [
          {
              id: 3,
              name: "form 1.1.1",
              description: "esto ya es un form comprado",
          },
          {
              id: 4,
              name: "form 1.1.2",
              description: "esto ya es un form comprado",
          }
        ]
      },
      {
        id: 3,
        name: "form 1.1",
        description: "esto ya es un form comprado",
      },
      {
        id: 4,
        name: "form 1.2",
        description: "esto ya es un form comprado",
      },
      {
        id: 5,
        name: "items 1.2",
        childrens: [
          {
              id: 6,
              name: "form 1.2.1",
              description: "esto ya es un form comprado",
          },
          {
              id: 7,
              name: "form 1.2.2",
              description: "esto ya es un form comprado",
          }
        ]
      }
    ]
  },
  {
    id: 8,
    name: "items 2",
    childrens: [
      {
        id: 9,
        name: "items 2.1",
        childrens: [
            {
              id: 10,
              name: "form 2.1.1",
              description: "esto ya es un form comprado",
            }
        ]
      },
      {
        id: 10,
        name: "form 2.1",
        description: "esto ya es un form comprado",
      }
    ]
  }
];

export default tree;