const itemsDriveD = [];
const itemsDriveC = [{
  id: '1',
  name: 'A.1',
  icon: 'activefolder',
  isDirectory: true,
  expanded: true,
  items: [ {
    id: '2',
    name: 'A.1.1',
    icon: 'activefolder',
    isDirectory: true,
    expanded: true,
    items: [ {
      id: '3',
      name: 'item A.1.1.1',
      icon: 'file',
      isDirectory: false
    }, {
      id: '4',
      name: 'item A.1.1.2',
      icon: 'file',
      isDirectory: false
    }
    ]
  }, {
    id: '5',
    name: 'item A.1.1',
    icon: 'file',
    isDirectory: false
  }, {
    id: '6',
    name: 'item A.1.2',
    icon: 'file',
    isDirectory: false
  }, {
    id: '7',
    name: 'item A.1.3',
    icon: 'file',
    isDirectory: false
  }
  ],
}, {
  id: '8',
  name: 'A.2',
  icon: 'activefolder',
  isDirectory: true,
  expanded: true,
  items: [ {
    id: '9',
    name: 'item A.2.1',
    icon: 'file',
    isDirectory: false
  }, {
    id: '10',
    name: 'item A.2.2',
    icon: 'file',
    isDirectory: false
  }
  ]
}, {
  id: '11',
  name: 'A.3',
  icon: 'activefolder',
  isDirectory: true,
  expanded: true,
  items: [ {
    id: '12',
    name: 'item A.3.1',
    icon: 'file',
    isDirectory: false
  }, {
    id: '13',
    name: 'item A.3.2',
    icon: 'file',
    isDirectory: false
  }
  ]
}, {
  id: '14',
  name: 'item A.1',
  icon: 'file',
  isDirectory: false
}, {
  id: '15',
  name: 'item A.2',
  icon: 'file',
  isDirectory: false
}, {
  id: '16',
  name: 'A.4',
  icon: 'activefolder',
  isDirectory: true,
  expanded: true,
  items: Array.from({ length: 1000 }, (v, k) => k).map((k) => ({
    id: 17 + k,
    name: `item A.4.${k}`,
    icon: 'file',
    isDirectory: false
  }))
}, {
  id: `${17 + 1000 + 1}`,
  name: 'A.5',
  icon: 'activefolder',
  isDirectory: true,
  expanded: true,
  items: [{
    id: `${17 + 1000 + 2}`,
    name: 'A.5.1',
    icon: 'activefolder',
    isDirectory: true,
    expanded: true,
    items: [{
      id: `${17 + 1000 + 3}`,
      name: 'A.5.1.1',
      icon: 'activefolder',
      isDirectory: true,
      expanded: true,
      items: [{
        id: `${17 + 1000 + 4}`,
        name: 'A.5.1.1.1',
        icon: 'activefolder',
        isDirectory: true,
        expanded: true,
        items: [{
          id: `${17 + 1000 + 5}`,
          name: 'A.5.1.1.1.1',
          icon: 'activefolder',
          isDirectory: true,
          expanded: true,
          items: Array.from({ length: 1000 }, (v, k) => k).map((k) => ({
            id: 17 + 1000 + 6 + k,
            name: `item A.5.${k}`,
            icon: 'file',
            isDirectory: false
          }))
        }]
      }]
    }]
  }]
}];

export default {
  getItemsDriveC() {
    return itemsDriveC;
  },
  getItemsDriveD() {
    return itemsDriveD;
  }
};
