import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { ChevronRight, ExpandMore, Search } from "@material-ui/icons";
import { TreeItem, TreeView } from "@material-ui/lab";

let droppables = {};

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  userSelect: "none",
  padding: grid,
  margin: `0 0 ${grid * 0.5}px 0`,
  background: isDragging ? "lightgreen" : "none",
  ...draggableStyle,
});

const getListStyle = () => ({
  padding: grid,
  width: 250,
});

const getDroppables = (tree, parent = undefined) => {
  for (const node in tree) {
    if (tree[node].childrens) {
      droppables = {
        ...droppables,
        [tree[node].name]: [],
      };
      getDroppables(tree[node].childrens, tree[node]);
    } else {
      droppables[parent.name].push(
        tree[node]
      );
    }
  }
};

const getSubtree = (root) => {
  return root.map((node) => {
    if (node && node.childrens) {
      return (
        <Droppable droppableId={node.name}>
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              style={getListStyle(snapshot.isDraggingOver)}
            >
              <TreeItem nodeId={node.id} label={node.name}>
                {getSubtree(node.childrens)}
                {provided.placeholder}
              </TreeItem>
            </div>
          )}
        </Droppable>
      );
    }
    return (
      <>
        {node && (
          <Draggable key={node.id} draggableId={node.id.toString()} index={node.id}>
            {(provided, snapshot) => (
              <div
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style={getItemStyle(
                  snapshot.isDragging,
                  provided.draggableProps.style
                )}
              >
                {node.name}
              </div>
            )}
          </Draggable>
        )}
      </>
    );
  });
};

const updateTree = (tree, source, destination, removed) => {
  for (const node in tree) {
    if (tree[node]?.childrens) {
      if (tree[node]?.name == destination) {
        console.log(tree[node]);
        console.log("destino", [...Object.values(tree[node].childrens), removed]);
        tree[node].childrens = [...Object.values(tree[node].childrens), removed];
      }
      if (tree[node]?.name == source) {
        console.log(tree[node]);
        console.log("source", tree[node].childrens.filter(x => x.id != removed?.id));
        tree[node].childrens = tree[node].childrens.filter(x => x.id != removed?.id);
      }
      updateTree(tree[node].childrens, source, destination, removed);
    }
  }
}

const TreeViewComponent = () => {
  const [tree, setTree] = useState([
    {
      id: 1,
      name: "items 1",
      childrens: [
        {
          id: 2,
          name: "items 1.1",
          childrens: [
            {
              id: 3,
              name: "form 1.1.1",
              description: "esto ya es un form comprado",
            },
            {
              id: 4,
              name: "form 1.1.2",
              description: "esto ya es un form comprado",
            },
          ],
        },
        {
          id: 7,
          name: "items 1.2",
          childrens: [
            {
              id: 8,
              name: "form 1.2.1",
              description: "esto ya es un form comprado",
            },
            {
              id: 9,
              name: "form 1.2.2",
              description: "esto ya es un form comprado",
            },
          ],
        },
        {
          id: 5,
          name: "form 1.1",
          description: "esto ya es un form comprado",
        },
        {
          id: 6,
          name: "form 1.2",
          description: "esto ya es un form comprado",
        }
      ],
    },
    {
      id: 10,
      name: "items 2",
      childrens: [
        {
          id: 11,
          name: "items 2.1",
          childrens: [
            {
              id: 12,
              name: "form 2.1.1",
              description: "esto ya es un form comprado",
            },
          ],
        },
        {
          id: 13,
          name: "form 2.1",
          description: "esto ya es un form comprado",
        },
      ],
    },
  ]);
  const [state, setState] = useState(droppables);

  useEffect(() => {
    getDroppables(tree);
    setState(droppables);
    console.log("droppables", droppables);
  }, []);

  const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.filter(x => x.id == droppableSource.index);
  
    sourceClone.splice(Object.keys(sourceClone.filter(x => x.id == droppableSource.index))[0], 1);
    destClone.splice(Object.keys(destClone.filter(x => x.id == droppableDestination.index))[0], 0, removed);
  
    console.log("tree", tree);
    console.log("removed", removed);
    console.log("source", source);
    console.log("destination", destination);
    console.log("sourceClone", sourceClone);
    console.log("destClone", destClone);
    console.log("droppableSource", droppableSource);
    console.log("droppableDestination", droppableDestination);
    let copyTree = Object.assign({}, tree);
    updateTree(copyTree, droppableSource.droppableId, droppableDestination.droppableId, removed);
    console.log("copyTree", copyTree);
    setTree(Object.values(copyTree));

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;
  
    return result;
  };

  const getList = (id) => state[id];

  const onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );

      let newState = { items };

      for (const droppable in Object.keys(state)) {
        if (source.droppableId === droppable) {
          newState = {...state, [droppable]: items};
        }
      }

      setState(newState);
    } else {
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );

      setState({
        ...state, ...result
      });
    }
  };

  console.log("state render", state);

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <TreeView
        defaultCollapseIcon={<ExpandMore />}
        defaultExpandIcon={<ChevronRight />}
      >
        {getSubtree(tree)}
      </TreeView>
    </DragDropContext>
  );
};

export default TreeViewComponent;
