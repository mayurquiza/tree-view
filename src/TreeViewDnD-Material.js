import React, { Component, useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { ChevronRight, ExpandMore } from "@material-ui/icons";
import { TreeItem, TreeView } from "@material-ui/lab";
import getTree from './tree';

// fake data generator
const getItems = (count, offset = 0) =>
  Array.from({ length: count }, (v, k) => k).map((k) => ({
    id: `item-${k + offset}`,
    content: `item ${k + offset}`,
  }));

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid,
  margin: `0 0 ${grid * 0.5}px 0`,

  // change background colour if dragging
  background: isDragging ? "lightgreen" : "none",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  // background: isDraggingOver ? 'lightblue' : 'lightgrey',
  padding: grid,
  width: 250,
});

const TreeViewComponent = () => {
  const [state, setState] = useState({
    droppable: getItems(4),
    droppable1: getItems(3, 4),
    droppable2: getItems(1, 7),
    droppable3: getItems(5, 8),
  });

  const getList = (id) => state[id];

  const onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );

      let newState = { items };

      if (source.droppableId === "droppable1") {
        newState = { ...state, droppable1: items };
      }

      if (source.droppableId === "droppable2") {
        newState = { ...state, droppable2: items };
      }

      if (source.droppableId === "droppable3") {
        newState = { ...state, droppable3: items };
      }

      setState(newState);
    } else {
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );

      setState({
        droppable: result.droppable ? result.droppable : state.droppable,
        droppable1: result.droppable1 ? result.droppable1 : state.droppable1,
        droppable2: result.droppable2 ? result.droppable2 : state.droppable2,
        droppable3: result.droppable3 ? result.droppable3 : state.droppable3,
      });
    }
  };

  console.log("state render", state);
  return (
    <DragDropContext onDragEnd={onDragEnd}>
      {/* un tree view por cada carpeta raiz */}

      <TreeView
        defaultCollapseIcon={<ExpandMore />}
        defaultExpandIcon={<ChevronRight />}
      >
        {/* un tree item por cada carpeta raiz */}
        <TreeItem nodeId="1" label="Items 1">
          {/* un droppable por cada carpeta hija */}
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
              <div
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}
              >
                {/* un tree item por cada carpeta hija */}
                <TreeItem nodeId="2" label="Items 1.1">
                  {/* un draggable por cada hijo que sea un item comprado */}
                  {state.droppable.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={getItemStyle(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          {item.content}
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </TreeItem>
              </div>
            )}
          </Droppable>
          {/* un droppable por cada carpeta hija */}
          <Droppable droppableId="droppable1">
            {(provided, snapshot) => (
              <div
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}
              >
                {/* un draggable por cada hijo que sea un item comprado */}
                {state.droppable1.map((item, index) => (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style
                        )}
                      >
                        {item.content}
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </TreeItem>
      </TreeView>

      {/* un tree view por cada carpeta raiz */}
      <TreeView
        defaultCollapseIcon={<ExpandMore />}
        defaultExpandIcon={<ChevronRight />}
      >
        {/* un tree item por cada carpeta raiz */}
        <TreeItem nodeId="3" label="Items 2">
          {/* un droppable por cada carpeta raiz */}
          <Droppable droppableId="droppable2">
            {(provided, snapshot) => (
              <div
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}
              >
                {/* un tree item por cada carpeta hija */}
                <TreeItem nodeId="4" label="Items 2.1">
                  {/* un draggable por cada hijo que sea un item comprado */}
                  {state.droppable2.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={getItemStyle(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          {item.content}
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </TreeItem>
              </div>
            )}
          </Droppable>
          {/* un droppable por cada carpeta raiz */}
          <Droppable droppableId="droppable3">
            {(provided, snapshot) => (
              <div
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}
              >
                {/* un draggable por cada hijo que sea un item comprado */}
                {state.droppable3.map((item, index) => (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style
                        )}
                      >
                        {item.content}
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </TreeItem>
      </TreeView>
    </DragDropContext>
  );
};

export default TreeViewComponent;
