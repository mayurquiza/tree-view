import React, { useState } from "react";
import "antd/dist/antd.css";
import "./index.css";
import { Tree } from "antd";
import { ArrowForward } from "@material-ui/icons";

// const x = 10;
// const y = 6;
// const z = 5;

const gData = [
  {
    title: "0",
    key: "0",
  },
  {
    title: "1",
    key: "1",
  },
  {
    title: "2",
    key: "2",
    isLeaf: true,
  },
];

const updateTreeData = (list, key, children) => {
  return list.map((node) => {
    if (node.key === key) {
      return { ...node, children };
    }

    if (node.children) {
      return {
        ...node,
        children: updateTreeData(node.children, key, children),
      };
    }

    return node;
  });
};

// const generateData = (_level, _preKey, _tns) => {
//   const preKey = _preKey || '0';
//   const tns = _tns || gData;

//   const children = [];
//   for (let i = 0; i < x; i++) {
//     const key = `${preKey}-${i}`;
//     tns.push({ title: key, key });
//     if (i < y) {
//       children.push(key);
//     }
//   }
//   if (_level < 0) {
//     return tns;
//   }
//   const level = _level - 1;
//   children.forEach((key, index) => {
//     tns[index].children = [];
//     return generateData(level, key, tns[index].children);
//   });
// };
// generateData(z);
console.log(gData);

const generateNodes = (quantity, key, leaf = false, index = 0) => {
    const nodes = [];
    for (let i = index; i < quantity + index; i++) {
        nodes.push({
            title: `${key}-${i}`,
            key: `${key}-${i}`,
            isLeaf: leaf,
        })
    }
    return nodes;
};

const TreeViewComponent = () => {
  const [treeData, setTreeData] = useState(gData);
  const [counter, setCounter] = useState(0);

  const onLoadData = ({ key, children }) => {
    return new Promise((resolve) => {
      if (children) {
        resolve();
        return;
      }

      if (counter === 5) {
        setTimeout(() => {
          setTreeData((origin) =>
            updateTreeData(origin, key, generateNodes(50, key, true))
          );
          resolve();
        }, 500);
      } else {
        setCounter(counter + 1);
        console.log("counter", counter);
        setTimeout(() => {
          setTreeData((origin) =>
            updateTreeData(origin, key, [
                ...generateNodes(16, key),
                ...generateNodes(30, key, true, 17)
            ])
          );
          resolve();
        }, 500);
      }
    });
  };

  const onDrop = (info) => {
    console.log("drop", info);
    if (!info.node.isLeaf || info.dropToGap) {

      const dropKey = info.node.key;
      const dragKey = info.dragNode.key;
      const dropPos = info.node.pos.split("-");

      const dropPosition =
        info.dropPosition - Number(dropPos[dropPos.length - 1]);

      const loop = (data, key, callback) => {
        for (let i = 0; i < data.length; i++) {
          if (data[i].key === key) {
            return callback(data[i], i, data);
          }
          if (data[i].children) {
            loop(data[i].children, key, callback);
          }
        }
      };
      const data = [...treeData];

      // Find dragObject
      let dragObj;
      loop(data, dragKey, (item, index, arr) => {
        arr.splice(index, 1);
        dragObj = item;
      });

      if (!info.dropToGap) {
        // Drop on the content
        loop(data, dropKey, (item) => {
          item.children = item.children || [];
          // where to insert 示例添加到头部，可以是随意位置
          item.children.unshift(dragObj);
        });
      } else if (
        (info.node.props.children || []).length > 0 && // Has children
        info.node.props.expanded && // Is expanded
        dropPosition === 1 // On the bottom gap
      ) {
        loop(data, dropKey, (item) => {
          item.children = item.children || [];
          // where to insert 示例添加到头部，可以是随意位置
          item.children.unshift(dragObj);
          // in previous version, we use item.children.push(dragObj) to insert the
          // item to the tail of the children
        });
      } else {
        let ar;
        let i;
        loop(data, dropKey, (item, index, arr) => {
          ar = arr;
          i = index;
        });
        if (dropPosition === -1) {
          ar.splice(i, 0, dragObj);
        } else {
          ar.splice(i + 1, 0, dragObj);
        }
      }

      setTreeData(data);
    }
  };

  return (
    <Tree
      className="draggable-tree"
      showLine={{showLeafIcon: false}}
      draggable
      onDrop={onDrop}
      treeData={treeData}
      loadData={onLoadData}
    />
  );
};

export default TreeViewComponent;
