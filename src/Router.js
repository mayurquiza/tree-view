import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
// import TreeViewComponent as TreeViewMaterial from './TreeViewDnD-Material';
import TreeViewMaterial from './TreeViewDnD-MaterialDynamic';
import TreeViewKendo from './TreeViewKendo';
import TreeViewDevExtreme from './TreeViewDevExtreme';
import TreeViewAnt from './TreeViewAnt';

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/material">Material + beautiful DnD</Link>
            </li>
            <li>
              <Link to="/kendo">Kendo</Link>
            </li>
            <li>
              <Link to="/devextreme">Dev Extreme</Link>
            </li>
            <li>
              <Link to="/ant">Ant</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path="/material">
            <TreeViewMaterial />
          </Route>
          <Route path="/kendo">
            <TreeViewKendo />
          </Route>
          <Route path="/devextreme">
            <TreeViewDevExtreme />
          </Route>
          <Route path="/ant">
            <TreeViewAnt />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
